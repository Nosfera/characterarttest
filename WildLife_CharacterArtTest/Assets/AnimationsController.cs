using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsController : MonoBehaviour
{
    [SerializeField] private Animator anim;
    private bool isCycling = false;

    public void CycleAnimation()
    {
        isCycling = !isCycling;
        anim.SetBool("Cycle", isCycling);
    }
}
